<?php
// PHP code to display an alert using JavaScript
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Alert Example</title>
</head>
<body>

<?php
// PHP code to output JavaScript alert
echo '<script type="text/javascript">alert("Hello, this is an alert from PHP!");</script>';
?>

</body>
</html>
